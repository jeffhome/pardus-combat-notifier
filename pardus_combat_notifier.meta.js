// ==UserScript==
// @name           Pardus Combat Notifier
// @namespace      http://userscripts.xcom-alliance.info/
// @description    Plays a sound when having unread combat notifications - based closely on the combat notifier from Takius Stargazer
// @author         Miche (Orion) / Sparkle (Artemis)
// @include        http*://*.pardus.at/msgframe.php
// @version        1.31
// @updateURL      http://userscripts.xcom-alliance.info/combat_notifier/pardus_combat_notifier.meta.js
// @downloadURL    http://userscripts.xcom-alliance.info/combat_notifier/pardus_combat_notifier.user.js
// @icon           http://userscripts.xcom-alliance.info/combat_notifier/icon.png
// @grant          none
// ==/UserScript==
